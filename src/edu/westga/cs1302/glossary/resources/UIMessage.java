package edu.westga.cs1302.glossary.resources;

/**
 * This class keeps track of a number of strings designed to be standard output
 * messages for the user
 * 
 * @author	CS1302
 * @version Fall 2021
 */
public class UIMessage {
	public static final String NULL_WORD = "Word cannot be null.";
	public static final String EMPTY_WORD = "Word cannot be empty.";
	public static final String NULL_DEFINITION = "Definition cannot be null.";
	public static final String EMPTY_DEFINITION = "Definition cannot be empty.";
	public static final String INVALID_NUMBER = "The importance number must be greater than 0.";
	public static final String NULL_COLLECTION = "Collection cannot be null.";
	public static final String COLLECTION_CANNOT_CONTAIN_NULL = "Collection cannot contain null.";
	public static final String NULL_TERM = "Term cannot be null.";
	public static final String DUPLICATE_TERM_CANNOT_BE_ADDED = "Duplicate term cannot be added.";
	public static final String NOT_TYPE_TERM = "The object is not of type Term.";
	public static final String SEARCH_PHRASE_CANNOT_BE_NULL = "The search phrase cannot be null";
}
