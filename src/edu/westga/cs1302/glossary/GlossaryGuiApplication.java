package edu.westga.cs1302.glossary;

import javafx.application.Application;
import javafx.stage.Stage;
import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;

/**
 * This class is the entry point into the Glossary application
 * 
 * @author	CS1302
 * @version Fall 2021
 */
public class GlossaryGuiApplication extends Application {

	private static final String WINDOW_TITLE = "Lab 8: Firstname Lastname";
	private static final String GUI_FXML = "view/GlossaryGUI.fxml";

	/**
	 * Constructs a new Application object for the Retail Inventory program.
	 * 
	 * @precondition none
	 * @postcondition the object is ready to execute
	 */
	public GlossaryGuiApplication() {
		super();
	}

	@Override
	public void start(Stage primaryStage) {
		try {
			Pane thePane = this.loadGui();
			Scene theScene = new Scene(thePane);
			primaryStage.setScene(theScene);
			primaryStage.setTitle(WINDOW_TITLE);
			primaryStage.show();
		} catch (IllegalStateException | IOException anException) {
			anException.printStackTrace();
		}
	}

	private Pane loadGui() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource(GUI_FXML));
		return (Pane) loader.load();
	}

	/**
	 * Launches the application.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param args not used
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
