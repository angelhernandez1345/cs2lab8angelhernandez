package edu.westga.cs1302.glossary.model;

import edu.westga.cs1302.glossary.resources.UIMessage;

/**
 * This class models a term learned in a course.
 * 
 * @author	CS1302
 * @version Fall 2021
 */
public class Term {

	private final String word;
	private final String definition;
	private final int importance;

	/**
	 * Creates a term.
	 * 
	 * @precondition word != null && word != empty && definition != null &&
	 *               definition != empty && importance > 0
	 * @postcondition getWord() == word && getDefinition() == definition &&
	 *                getImportance() == importance
	 * @param word       the word representing this term
	 * @param definition the term's definition
	 * @param importance the importance of this term as a number
	 */
	public Term(String word, String definition, int importance) {
		if (word == null) {
			throw new IllegalArgumentException(UIMessage.NULL_DEFINITION);
		}
		if (word.length() == 0) {
			throw new IllegalArgumentException(UIMessage.EMPTY_DEFINITION);
		}

		if (definition == null) {
			throw new IllegalArgumentException(UIMessage.NULL_WORD);
		}
		if (definition.length() == 0) {
			throw new IllegalArgumentException(UIMessage.EMPTY_DEFINITION);
		}

		if (importance <= 0) {
			throw new IllegalArgumentException(UIMessage.INVALID_NUMBER);
		}
		this.word = word;
		this.definition = definition;
		this.importance = importance;
	}

	/**
	 * Returns the word (String) of this term.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the word
	 */
	public String getWord() {
		return this.word;
	}

	/**
	 * Returns the definition of this term.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the definition
	 */
	public String getDefinition() {
		return this.definition;
	}

	/**
	 * Returns the importance of this term.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the importance
	 */
	public int getImportance() {
		return this.importance;
	}

}
