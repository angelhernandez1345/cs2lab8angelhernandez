package edu.westga.cs1302.glossary.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import edu.westga.cs1302.glossary.resources.UIMessage;

/**
 * Keeps track of a collection of terms in a glossary.
 * 
 * @author	CS1302
 * @version Fall 2021
 */
public class Glossary implements Collection<Term> {

	private final List<Term> terms;

	/**
	 * Creates a new Glossary by setting it up to accept new terms.
	 * 
	 * @precondition none
	 * @postcondition a new glossary with size() == 0
	 */
	public Glossary() {
		this.terms = new ArrayList<Term>();
	}

	/**
	 * Creates a new glossary and populates it with the specified terms.
	 * 
	 * @precondition startingTerms != null
	 * @postcondition a new glossary with all the specified terms included
	 * @param startingTerms the terms to be added to the glossary
	 */
	public Glossary(Collection<Term> startingTerms) {
		if (startingTerms == null) {
			throw new IllegalArgumentException(UIMessage.NULL_COLLECTION);
		}
		this.terms = new ArrayList<Term>();
		this.terms.addAll(startingTerms);
	}

	@Override
	public int size() {
		return this.terms.size();
	}

	@Override
	public boolean isEmpty() {
		return this.terms.isEmpty();
	}

	@Override
	public boolean contains(Object term) {
		if (term == null) {
			throw new IllegalArgumentException(UIMessage.NULL_TERM);
		}
		return this.terms.contains(term);
	}

	@Override
	public Iterator<Term> iterator() {
		return this.terms.iterator();
	}

	@Override
	public Object[] toArray() {
		return this.terms.toArray();
	}

	@Override
	public <T> T[] toArray(T[] terms) {
		return this.terms.toArray(terms);
	}

	/**
	 * Adds the specified term to the glossary; does not allow duplicates.
	 * 
	 * @precondition term != null
	 * @postcondition term is added to glossary && size() == size()@prev + 1
	 * @param term the new term to be added
	 * @return true, if successful; false otherwise
	 */
	@Override
	public boolean add(Term term) {
		if (term == null) {
			throw new IllegalArgumentException(UIMessage.NULL_TERM);
		}

		if (this.terms.contains(term)) {
			return false;
		}
		return this.terms.add(term);
	}

	@Override
	public boolean remove(Object term) {
		if (term == null) {
			throw new IllegalArgumentException(UIMessage.NULL_TERM);
		}
		return this.terms.remove(term);
	}

	@Override
	public boolean containsAll(Collection<?> termCollection) {
		if (termCollection == null) {
			throw new IllegalArgumentException(UIMessage.NULL_COLLECTION);
		}
		if (termCollection.contains(null)) {
			throw new IllegalArgumentException(UIMessage.COLLECTION_CANNOT_CONTAIN_NULL);
		}
		return this.terms.containsAll(termCollection);
	}

	@Override
	public boolean addAll(Collection<? extends Term> termCollection) {
		if (termCollection == null) {
			throw new IllegalArgumentException(UIMessage.NULL_COLLECTION);
		}

		boolean result = false;
		for (Term currentTerm : termCollection) {
			if (this.terms.contains(currentTerm)) {
				throw new IllegalArgumentException(UIMessage.DUPLICATE_TERM_CANNOT_BE_ADDED);
			} else {
				this.terms.add(currentTerm);
				result = true;
			}
		}
		return result;
	}

	@Override
	public boolean removeAll(Collection<?> termCollection) {
		if (termCollection == null) {
			throw new IllegalArgumentException(UIMessage.NULL_COLLECTION);
		}
		return this.terms.removeAll(termCollection);
	}

	@Override
	public boolean retainAll(Collection<?> termCollection) {
		if (termCollection == null) {
			throw new IllegalArgumentException(UIMessage.NULL_COLLECTION);
		}
		return this.terms.retainAll(termCollection);
	}

	@Override
	public void clear() {
		this.terms.clear();
	}
}
