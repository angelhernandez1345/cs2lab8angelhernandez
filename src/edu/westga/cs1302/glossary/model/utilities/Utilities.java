package edu.westga.cs1302.glossary.model.utilities;

import java.util.ArrayList;
import java.util.List;

import edu.westga.cs1302.glossary.model.Term;

/**
 * The Utilities class.
 * 
 * @author	CS1302
 * @version Fall 2021
 */
public class Utilities {

	/**
	 * Returns a list of terms.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return list of terms
	 */
	public static List<Term> loadTerms() {

		List<Term> terms = new ArrayList<Term>();

		terms.add(new Term("API", "application programming interface", 5));
		terms.add(new Term("Abstract Class",
				"An abstract class is a class that is declared abstract - "
						+ "it may or may not include abstract methods. Abstract classes "
						+ "cannot be instantiated, but they can be subclassed.",
				50));
		terms.add(new Term("Abstract Method", "An abstract method is a method that is declared without an "
				+ "implementation (without braces, and followed by a semicolon).", 50));
		terms.add(new Term("GUI", "Graphical User Interface", 1));
		terms.add(new Term("Interface",
				"In the Java programming language, an interface is a reference "
						+ "type, similar to a class, that can contain only constants, "
						+ "method signatures, default methods, static methods, and nested types. "
						+ "Method bodies exist only for default methods and static methods. "
						+ "Interfaces cannot be instantiated — they can only be implemented "
						+ "by classes or extended by other interfaces.",
				100));
		terms.add(new Term("Method Signature", "Name, plus the number and the type of its parameters.", 3));
		terms.add(new Term("The class \"Object\"",
				"The Object class is the top of the class hierarchy. " + "All classes "
						+ "are descendants from this class and inherit methods from it. "
						+ "Useful methods inherited from Object include toString(), equals(), clone(), and getClass().",
				25));
		terms.add(new Term("Polymorphism",
				"The dictionary definition of polymorphism refers to a principle"
						+ " in biology in which an organism or species can have many "
						+ "different forms or stages. This principle can also be applied to "
						+ "object-oriented programming and languages like the Java language. "
						+ "Subclasses of a class can define their own unique behaviors and "
						+ "yet share some of the same functionality of the parent class.",
				100));

		return terms;
	}

}
